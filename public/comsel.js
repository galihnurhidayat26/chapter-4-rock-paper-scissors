//computer selection
export class comsel {
    constructor(comp) {
        this.comp = comp
    }

    getcomputerSelection() {
        let hasilSuit = null
        if (this.comp >= 0 && this.comp < 0.33) {
            hasilSuit = 'kertas'
        }
        else if (this.comp >= 0.33 && this.comp < 0.67) {
            hasilSuit = 'gunting'
        }
        else if (this.comp >= 0.67 && this.comp <= 1) {
            hasilSuit = 'batu'
        }
        document.querySelector('.COM .' + hasilSuit).style.backgroundColor = '#757575';
        return hasilSuit
    }
}
