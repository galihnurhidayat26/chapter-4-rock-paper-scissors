/// Player Selection
import { comsel } from './comsel.js'
function mulaiGame() {
    const select = document.querySelectorAll('li img')//untuk menandakan element yg ditunjuk (lihat css).queryselectorall 
    select.forEach(function (sel) {
        sel.addEventListener('click', function () {
            resetCOMColor();
            const playerSelection = sel.className
            const comsel1 = new comsel(Math.random())
            const computerSel = comsel1.getcomputerSelection()
            const computerSelection = computerSel
            const result = getResult(playerSelection, computerSelection)
            console.log(`\nPLAYER: ${playerSelection}\nCOM: ${computerSelection}\nResult: ${result}`)
            const gameResult = document.querySelector('.hasil-game')
            gameResult.innerHTML = result
        })
    })
    function resetCOMColor() {
        document.querySelector('.COM .batu').style.backgroundColor = "#9c835f";
        document.querySelector('.COM .kertas').style.backgroundColor = "#9c835f";
        document.querySelector('.COM .gunting').style.backgroundColor = "#9c835f";
    }

    //Game Result
    function getResult(player, comp) {
        if (player === comp) {
            return 'DRAW'
        }
        if (player === 'batu') {
            return (comp === 'kertas') ? 'COM WIN!' : 'PLAYER WIN!'
        }
        if (player === 'kertas') {
            return (comp === 'batu') ? 'PLAYER WIN!' : 'COM WIN!'
        }
        if (player === 'gunting') {
            return (comp === 'batu') ? 'COM WIN!' : 'PLAYER WIN!'
        }
    }
    return true
}
mulaiGame()

document.getElementsByName('refresh')
document.addEventListener('click', () => {
})